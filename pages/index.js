import Head from 'next/head';
import styles from '../styles/Home.module.css';

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <p>Deep links</p>
      <ul>
        <li>
          <a href="https://gazin.page.link/?link=https://www.gazin.com.br/conta/pedido/233684&ofl=https://www.gazin.com.br/conta/pedidos&apn=com.b2capp&amv=105&isi=1541397077&ibi=br.com.gazin.b2capp&st=Acompanhe+o+status+do+seu+pedido">
            Open Android
          </a>
        </li>
        <li>
          <a href="gazin://gazin/produto/smartphone-samsung-galaxy-a11-64-octa-core-64gb-3gb-camera-tripla/2422">
            Open Android Produto 2422
          </a>
        </li>
        <li>
          <a href="gazin://gazin/produto/aspirador-vertical-britania-dust-off-po-1000w/1161/preto/110-volts">
            Open Android Produto 1161 Preto 110v
          </a>
        </li>
        <li>
          <a href="gazin://gazin/produto/aspirador-vertical-britania-dust-off-po-1000w/1161/vermelho/220-volts">
            Open Android Produto 1161 Vermelho 220v
          </a>
        </li>
        <li>
          <a href="gazin://gazin/buscar/search/Geladeira_Brastemp/?slug=geladeira_brastemp">
            Open Android Buscar
          </a>
        </li>
        <li>
          <a href="gazin://gazin/buscar/departments/Super_Promoção/?categories=kits-de-instalacao">
            Open Android Buscar Categoria
          </a>
        </li>
        <li>
          <a href="gazin://gazin/buscar/hotsite/Mais_Vendidos/?slug=mostviewed">
            Open Android Buscar Hotsite
          </a>
        </li>
        <li>
          <a href="gazin://gazin/carrinho">Open Android Carrinho</a>
        </li>
        <li>
          <a href="gazin://gazin/livelo">Open Android Livelo</a>
        </li>
        <li>
          <a href="gazin://gazin/hotsites/teste-app">Open Android Hotsite</a>
        </li>
        <li>
          <a href="market://details?id=com.b2capp">
            Open Android Playstore Gazin
          </a>
        </li>
        <li>
          <a href="gazin://">Open iOS</a>
        </li>
        <li>
          <a href="gazin://produto/smartphone-samsung-galaxy-a11-64-octa-core-64gb-3gb-camera-tripla/2422">
            Open iOS Produto 2422
          </a>
        </li>
        <li>
          <a href="gazin://produto/aspirador-vertical-britania-dust-off-po-1000w/1161/preto/110-volts">
            Open iOS Produto 1161 Preto 110v
          </a>
        </li>
        <li>
          <a href="gazin://livelo">Open iOS Livelo</a>
        </li>
        <li>
          <a href="gazin://hotsites/teste-app">Open iOS Hotsite</a>
        </li>
        <li>
          <a href="itms-apps://itunes.apple.com/br/app/gazin-compras-online/id1541397077">
            Open iOS Appstore Gazin
          </a>
        </li>
      </ul>

      <p>Resultados google (Simulação de App Links)</p>
      <ul>
        <li>
          <a href="https://www.gazin.com.br">Gazin</a>
        </li>
        <li>
          <a href="https://www.gazin.com.br/produto/forno-eletrico-de-bancada-42l-suggar-com-funcao-grill-e-timer-fe420/7722/preto/220-volts">
            Gazin Produto
          </a>
        </li>
        <li>
          <a href="https://www.gazin.com.br/categoria/moveis">
            Categoria Test (nao deve abrir)
          </a>
        </li>
        <li>
          <a href="https://www.gazin.com.br/livelo">Open Livelo</a>
        </li>
        <li>
          <a href="https://staging.gazin.com.br/livelo">Open Stage Livelo</a>
        </li>
        <li>
          <a href="https://www.gazin.com.br/hotsites/ofertasexpoinga">
            Open Prod Hotsite
          </a>
        </li>
        <li>
          <a href="https://staging.gazin.com.br/hotsites/expoinga">
            Open Stage Hotsite
          </a>
        </li>
        <li>
          <a href="https://staging.gazin.com.br/busca/162582.86.1">
            Open Busca 162582.86.1
          </a>
        </li>
        <li>
          <a href="https://staging.gazin.com.br/categoria/celulares-e-smartphones">
            Open Categoria celulares-e-smartphones
          </a>
        </li>
      </ul>
    </div>
  );
}
